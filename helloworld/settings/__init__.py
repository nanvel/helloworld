from .default import *
from .db import *
from .media import *
from .apps import *
from .logging import *

try:
    # load custom settings
    from .local import *
except ImportError:
    pass