Hello world project
===================

Usage
-----

.. code-block:: bash

    git clone git@bitbucket.org:nanvel/helloworld.git
    cd helloworld
    virtualemv .env --no-site-packages
    source .env/bin/activate
    pip install -r requirements.txt
    python manage.py syncdb
    python manage.py runserver
    # navigate to http://127.0.0.1:8000